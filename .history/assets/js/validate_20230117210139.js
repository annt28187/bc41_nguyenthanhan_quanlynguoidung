function kiemTraTrung(userND, ndArr) {
  var viTri = timKiemViTri(userND, ndArr);
  if (viTri != -1) {
    showMessageError('tbTaiKhoan', 'Tài khoản nhân viên đã tồn tại');
    getID('tbTaiKhoan').style.display = 'block';
    return false;
  } else {
    showMessageError('tbTaiKhoan', '');
    getID('tbTaiKhoan').style.display = 'none';
    return true;
  }
}

function kiemTraRong(value, idError, message) {
  if (value.length == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraDoDai(value, idError, minLength, maxLength) {
  var length = value.length;
  if (length < minLength || length > maxLength) {
    getID(idError).innerText = `Giá trị từ ${minLength} đến ${maxLength}`;
    getID(idError).style.display = 'block';
    return false;
  } else {
    getID(idError).innerText = '';
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraChu(value, idError, message) {
  // var regLetter = /^[A-Za-z]+$/;
  var regLetter =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isLetter = regLetter.test(value);
  if (isLetter) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraSo(value, idError, message) {
  var regNumber = /^\d+$/;
  var isNumber = regNumber.test(value);
  if (isNumber) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraEmail(value) {
  var regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regEmail.test(value);
  if (isEmail) {
    showMessageError('tbEmailNV', '');
    getID('tbEmailNV').style.display = 'none';
    return true;
  } else {
    showMessageError('tbEmailNV', 'Email không hợp lệ');
    getID('tbEmailNV').style.display = 'block';
    return false;
  }
}

function kiemTraChucVu() {
  var tagSelect = getID('chucVu');
  if (tagSelect.selectedIndex == 0) {
    showMessageError('tbChucVu', 'Bạn chưa chọn chức vụ');
    getID('tbChucVu').style.display = 'block';
    return false;
  } else {
    showMessageError('tbChucVu', '');
    getID('tbChucVu').style.display = 'none';
    return true;
  }
}

function kiemTraMatKhau(value) {
  var regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,8}$/;
  if (value.match(regPass)) {
    showMessageError('tbMatKhauNV', '');
    getID('tbMatKhauNV').style.display = 'none';
    return true;
  } else {
    showMessageError(
      'tbMatKhauNV',
      'Mật khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)'
    );
    getID('tbMatKhauNV').style.display = 'block';
    return false;
  }
}

function convertString(maxLength, value) {
  if (value.length > maxLength) {
    return value.slice(0, maxLength) + '...';
  } else {
    return value;
  }
}
