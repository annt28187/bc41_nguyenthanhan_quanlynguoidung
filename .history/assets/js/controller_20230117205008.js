function enableLoading() {
  document.getElementById('loading').style.display = 'flex';
}

function disableLoading() {
  document.getElementById('loading').style.display = 'none';
}

function renderUsersList(userArr) {
  var contentHTML = '';
  userArr.forEach(function (user) {
    var contentTR = `<tr>
                        <td>${user.id}</td>
                        <td>${user.taiKhoan}</td>
                        <td>${user.matKhau}</td>
                        <td>${user.hoTen}</td>
                        <td>${user.email}</td>
                        <td>${
                          user.ngonNgu == 'ITALIAN'
                            ? "<span class='text-primary'>ITALIAN</span>"
                            : user.ngonNgu == 'FRENCH'
                            ? "<span class='text-success'>FRENCH</span>"
                            : user.ngonNgu == 'JAPANESE'
                            ? "<span class='text-info'>JAPANESE</span>"
                            : user.ngonNgu == 'CHINESE'
                            ? "<span class='text-danger'>CHINESE</span>"
                            : user.ngonNgu == 'RUSSIAN'
                            ? "<span class='text-warning'>RUSSIAN</span>"
                            : user.ngonNgu == 'SWEDEN'
                            ? "<span class='text-muted'>SWEDEN</span>"
                            : "<span class='text-body'>SPANISH</span>"
                        }</td>
                        <td>${
                          user.loaiND
                            ? "<span class='text-primary'>Giáo viên</span>"
                            : "<span class='text-success'>Học viên</span>"
                        }</td>
                        <td> <button onclick="xoaNguoiDung(${
                          user.id
                        })" class="btn btn-danger">Xoá</button> 
                        <button onclick="suaNguoiDung(${
                          user.id
                        })"class="btn btn-warning">Sửa</button>
                      </td>
                    </tr>`;
    contentHTML += contentTR;
  });
  document.getElementById('tblDanhSachNguoiDung').innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var taiKhoan = document.getElementById('taiKhoan').value;
  var hoTen = document.getElementById('hoTen').value;
  var matKhau = document.getElementById('matKhau').value;
  var email = document.getElementById('email').value;
  var hinhAnh = document.getElementById('hinhAnh').value;
  var loaiNguoiDung = document.getElementById('loaiNguoiDung').value;
  var loaiNgonNgu = document.getElementById('loaiNgonNgu').value;
  var moTa = document.getElementById('moTa').value;
  var user = {
    taiKhoan: taiKhoan,
    hoTen: hoTen,
    matKhau: matKhau,
    email: email,
    hinhAnh: hinhAnh,
    loaiND: loaiNguoiDung,
    ngonNgu: loaiNgonNgu,
    moTa: moTa,
  };
  return user;
}

function hienThiThongTinLenForm(user) {
  document.getElementById('taiKhoan').value = user.taiKhoan;
  document.getElementById('hoTen').value = user.hoTen;
  document.getElementById('matKhau').value = user.matKhau;
  document.getElementById('email').value = user.email;
  document.getElementById('hinhAnh').value = user.hinhAnh;
  document.getElementById('loaiNguoiDung').value = user.loaiND;
  document.getElementById('loaiNgonNgu').value = user.ngonNgu;
  document.getElementById('moTa').value = user.moTa;
}
