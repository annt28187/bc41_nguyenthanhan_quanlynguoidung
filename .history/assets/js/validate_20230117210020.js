function kiemTraTrung(userND, ndArr) {
  var viTri = timKiemViTri(userND, ndArr);
  if (viTri != -1) {
    showMessageError('tbTaiKhoan', 'Tài khoản nhân viên đã tồn tại');
    getID('tbTaiKhoan').style.display = 'block';
    return false;
  } else {
    showMessageError('tbTaiKhoan', '');
    getID('tbTaiKhoan').style.display = 'none';
    return true;
  }
}

function kiemTraRong(value, idError, message) {
  if (value.length == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraDoDai(value, idError, minLength, maxLength) {
  var length = value.length;
  if (length < minLength || length > maxLength) {
    getID(idError).innerText = `Giá trị từ ${minLength} đến ${maxLength}`;
    getID(idError).style.display = 'block';
    return false;
  } else {
    getID(idError).innerText = '';
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraChu(value, idError, message) {
  // var regLetter = /^[A-Za-z]+$/;
  var regLetter =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isLetter = regLetter.test(value);
  if (isLetter) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraSo(value, idError, message) {
  var regNumber = /^\d+$/;
  var isNumber = regNumber.test(value);
  if (isNumber) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraEmail(value) {
  var regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regEmail.test(value);
  if (isEmail) {
    showMessageError('tbEmailNV', '');
    getID('tbEmailNV').style.display = 'none';
    return true;
  } else {
    showMessageError('tbEmailNV', 'Email không hợp lệ');
    getID('tbEmailNV').style.display = 'block';
    return false;
  }
}

function ngayHopLe(value) {
  var regDay = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
  if (!regDay.test(value)) return false;

  var parts = value.split('/');
  var ngay = parseInt(parts[1], 10);
  var thang = parseInt(parts[0], 10);
  var nam = parseInt(parts[2], 10);

  if (nam < 1000 || nam > 3000 || thang == 0 || thang > 12) return false;

  var thangArr = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  if (nam % 400 == 0 || (nam % 100 != 0 && nam % 4 == 0)) thangArr[1] = 29;

  return ngay > 0 && ngay <= thangArr[thang - 1];
}

function kiemTraNgay() {
  var ngayVaoLam = getID('datepicker').value;
  if (ngayHopLe(ngayVaoLam)) {
    showMessageError('tbNgayVaoLam', '');
    getID('tbNgayVaoLam').style.display = 'none';
    return true;
  } else {
    showMessageError('tbNgayVaoLam', 'Ngày vào làm không hợp lệ');
    getID('tbNgayVaoLam').style.display = 'block';
    return false;
  }
}

function kiemTraChucVu() {
  var tagSelect = getID('chucVu');
  if (tagSelect.selectedIndex == 0) {
    showMessageError('tbChucVu', 'Bạn chưa chọn chức vụ');
    getID('tbChucVu').style.display = 'block';
    return false;
  } else {
    showMessageError('tbChucVu', '');
    getID('tbChucVu').style.display = 'none';
    return true;
  }
}

function kiemTraMatKhau(value) {
  var regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/;
  if (value.match(regPass)) {
    showMessageError('tbMatKhauNV', '');
    getID('tbMatKhauNV').style.display = 'none';
    return true;
  } else {
    showMessageError(
      'tbMatKhauNV',
      'Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)'
    );
    getID('tbMatKhauNV').style.display = 'block';
    return false;
  }
}

function convertString(maxLength, value) {
  if (value.length > maxLength) {
    return value.slice(0, maxLength) + '...';
  } else {
    return value;
  }
}
